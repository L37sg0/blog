<?php

namespace L37sg0\Blog\Http\Controllers;

use Illuminate\Routing\Controller;

class FrontendController extends Controller
{
    public function index() {
        return view('blogapp::landing');
    }
}
