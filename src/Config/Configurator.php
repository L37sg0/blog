<?php

namespace L37sg0\Blog\Config;

class Configurator
{
    public static function admin() {
        return [

            /*
            |--------------------------------------------------------------------------
            | Jetstream Stack
            |--------------------------------------------------------------------------
            |
            | This configuration value informs Jetstream which "stack" you will be
            | using for your application. In general, this value is set for you
            | during installation and will not need to be changed after that.
            |
            */

            'stack' => 'livewire',

            /*
             |--------------------------------------------------------------------------
             | Jetstream Route Middleware
             |--------------------------------------------------------------------------
             |
             | Here you may specify which middleware Jetstream will assign to the routes
             | that it registers with the application. When necessary, you may modify
             | these middleware; however, this default value is usually sufficient.
             |
             */

            'middleware' => ['web', 'auth:sanctum', 'verified'],

            /*
            |--------------------------------------------------------------------------
            | Jetstream Guard
            |--------------------------------------------------------------------------
            |
            | Here you may specify the authentication guard Jetstream will use while
            | authenticating users. This value should correspond with one of your
            | guards that is already present in your "auth" configuration file.
            |
            */

            'guard' => 'sanctum',

            'prefix' => 'dashboard/blogapp'

        ];
    }

    public static function frontend() {
        return [
            'prefix' => 'blogapp'
        ];
    }

}
