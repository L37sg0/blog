<?php

namespace L37sg0\Blog\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use L37sg0\Blog\Config\Configurator;

class BlogServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    /**
     * @return void
     */
    public function boot()
    {
        $this->registerRoutes();
        $this->registerViews();
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfigurationAdmin(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../../routes/admin.php');
        });
        Route::group($this->routeConfigurationFrontend(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../../routes/frontend.php');
        });
    }

    protected function routeConfigurationAdmin()
    {
        return Configurator::admin();
    }

    protected function routeConfigurationFrontend()
    {
        return Configurator::frontend();
    }

    protected function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'blogapp');
    }

}
