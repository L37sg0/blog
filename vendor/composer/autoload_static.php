<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2c12dde4e4b43e5be55d17dd1e5ff4bd
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'L37sg0\\Blog\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'L37sg0\\Blog\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2c12dde4e4b43e5be55d17dd1e5ff4bd::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2c12dde4e4b43e5be55d17dd1e5ff4bd::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit2c12dde4e4b43e5be55d17dd1e5ff4bd::$classMap;

        }, null, ClassLoader::class);
    }
}
